## Détails
- **Nom de la fonctionnalité**: `FEATURE_NAME`

- Lancer les tests de guidance sur chaques fonctions du module:
    - [ ] Model
    - [ ] Vue
    - [ ] Controller
    
- Vérifier le code coverage (Supérieur à 75% pour etre accepter en production) 
   
        
